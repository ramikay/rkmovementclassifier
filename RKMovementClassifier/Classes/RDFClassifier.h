//
//  RDFClassifier.h
//  RKMovementTest
//
//  Created by Rami on 26/3/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RKMovementConstants.h"

@interface RDFClassifier : NSObject

- (id) initWithLearningSamples:(double *)theLearningSamples
               numberOfSamples:(int)nSamples
              numberOfFeatures:(int)nFeatures
               numberOfClasses:(int)nClasses
                 numberOfTrees:(int)nTrees
                         ratio:(double)theRatio;

- (NSArray *) classifyData:(NSArray *)theData;

@end
