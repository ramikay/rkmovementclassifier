//
//  RKArrayUtils.m
//  RKMovementTest
//
//  Created by Rami on 8/4/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import "RKArrayUtils.h"

double max_array(double a[], int num_elements)
{
    double max=DBL_MIN;
    
    for (int i=0; i<num_elements; i++)
    {
        if (a[i]>max)
        {
            max=a[i];
        }
    }
    return(max);
}

double min_array(double a[], int num_elements)
{
    double min=DBL_MAX;
    
    for (int i=0; i<num_elements; i++)
    {
        if (a[i]<min)
        {
            min=a[i];
        }
    }
    return(min);
}

double avg_array(double a[], int num_elements)
{
    double avg=0;
    
    for (int i=0; i<num_elements; i++)
    {
        avg += a[i];
    }
    
    avg = avg / num_elements;
    
    return(avg);
}

double std_dev_array(double a[], int num_elements, double mean)
{
    double standardDeviation = 0.0;
    double sumOfSquaredDifferences = 0.0;
    
    for (int i=0; i<num_elements; i++)
    {
        double difference = a[i] - mean;
        sumOfSquaredDifferences += pow(difference, 2);
    }
    
    standardDeviation = sqrt(sumOfSquaredDifferences/num_elements);
    
    return(standardDeviation);
}
