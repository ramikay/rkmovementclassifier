//
//  RKMovementClassifier.m
//  RKMovementTest
//
//  Created by Rami on 7/4/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import <CoreMotion/CoreMotion.h>
#import "RKMovementConstants.h"
#import "RKMovementClassifier.h"
#import "RDFClassifier.h"
#import "RKTrainingSet.h"
#import "RKArrayUtils.h"

@interface RKMovementClassifier ()
{
    @private
    RDFClassifier *classifier;
    RKMovementHandlerBlock handlerBlock;

    CMMotionManager *motionManager;
    NSOperationQueue *motionQueue;

    int counter;
    
    BOOL detecting;
    NSLock *detectionLock;

    double accelerationMagnitudes[FEATURE_SET_SIZE];
    double eulerAnglesMagnitudes[FEATURE_SET_SIZE];
    double rotationRateMagnitudes[FEATURE_SET_SIZE];
}

- (void) handleMotion:(CMDeviceMotion *)theMotion error:(NSError *)theError;

- (void) setDetection:(BOOL)inProgress;

-(void) beginMovementDetection;
-(void) endMovementDetection;
@end

@implementation RKMovementClassifier

//------------------------------------------
// Singleton methods
//------------------------------------------
#pragma mark - Singleton methods

+ (RKMovementClassifier *) sharedInstance
{
    static dispatch_once_t pred;
    static RKMovementClassifier *_shared = nil;
    
    dispatch_once(&pred, ^{
        _shared = [[RKMovementClassifier alloc] init];
    });
    
    return _shared;
}

//------------------------------------------
// Init
//------------------------------------------
#pragma mark - Init

- (id) init
{
    self = [super init];
    
    if (self)
    {
        motionManager = [[CMMotionManager alloc] init];
        motionManager.deviceMotionUpdateInterval = UPDATE_INTERVAL;
        
        motionQueue = [[NSOperationQueue alloc] init];
        [motionQueue setMaxConcurrentOperationCount:1];
    }
    
    return self;
}

//------------------------------------------
// Private methods
//------------------------------------------
#pragma mark - Private methods

- (void) handleMotion:(CMDeviceMotion *)theMotion error:(NSError *)theError
{
    CMAcceleration acceleration = theMotion.userAcceleration;
    CMRotationRate rotation = theMotion.rotationRate;
    CMAttitude *attitude = [theMotion.attitude copy];
    
    counter++;
    
    accelerationMagnitudes[counter] = sqrt(pow(acceleration.x, 2) + pow(acceleration.y, 2) + pow(acceleration.z, 2));
    eulerAnglesMagnitudes[counter] = sqrt(pow(attitude.roll,2) + pow(attitude.pitch,2) + pow(attitude.yaw,2));
    rotationRateMagnitudes[counter] = sqrt(pow(rotation.x,2) + pow(rotation.y,2) + pow(rotation.z,2));
    
    if (counter == FEATURE_SET_SIZE)
    {
        NSNumber *maxAcceleration = [NSNumber numberWithDouble:max_array(accelerationMagnitudes, FEATURE_SET_SIZE)];
        NSNumber *minAcceleration = [NSNumber numberWithDouble:min_array(accelerationMagnitudes, FEATURE_SET_SIZE)];
        NSNumber *avgAcceleration = [NSNumber numberWithDouble:avg_array(accelerationMagnitudes, FEATURE_SET_SIZE)];
        NSNumber *stdAcceleration = [NSNumber numberWithDouble:std_dev_array(accelerationMagnitudes, FEATURE_SET_SIZE, [avgAcceleration doubleValue])];
        
        NSNumber *maxEulerAngles = [NSNumber numberWithDouble:max_array(eulerAnglesMagnitudes, FEATURE_SET_SIZE)];
        NSNumber *minEulerAngles = [NSNumber numberWithDouble:min_array(eulerAnglesMagnitudes, FEATURE_SET_SIZE)];
        NSNumber *avgEulerAngles = [NSNumber numberWithDouble:avg_array(eulerAnglesMagnitudes, FEATURE_SET_SIZE)];
        NSNumber *stdEulerAngles = [NSNumber numberWithDouble:std_dev_array(eulerAnglesMagnitudes, FEATURE_SET_SIZE, [avgEulerAngles doubleValue])];
        
        NSNumber *maxRotationRate = [NSNumber numberWithDouble:max_array(rotationRateMagnitudes, FEATURE_SET_SIZE)];
        NSNumber *minRotationRate = [NSNumber numberWithDouble:min_array(rotationRateMagnitudes, FEATURE_SET_SIZE)];
        NSNumber *avgRotationRate = [NSNumber numberWithDouble:avg_array(rotationRateMagnitudes, FEATURE_SET_SIZE)];
        NSNumber *stdRotationRate = [NSNumber numberWithDouble:std_dev_array(rotationRateMagnitudes, FEATURE_SET_SIZE, [avgRotationRate doubleValue])];
        
        NSArray *features = [[NSArray alloc] initWithObjects:minAcceleration, maxAcceleration, avgAcceleration, stdAcceleration,
                                                             minEulerAngles, maxEulerAngles, avgEulerAngles, stdEulerAngles,
                                                             minRotationRate, maxRotationRate, avgRotationRate, stdRotationRate,
                                                             nil];
        
        NSArray *probabilities = [classifier classifyData:features];
        
        double max = INT_MIN;
        RKMovementType class = -1;
        
        for (int i = 0; i < [probabilities count]; i++)
        {
            double value = [[probabilities objectAtIndex:i] doubleValue];
            if (value > max)
            {
                max = value;
                class = i;
            }
        }
        
        if (handlerBlock != nil)
        {
            handlerBlock(probabilities, class);
        }
        
        [self endMovementDetection];
    }
}

-(void) beginMovementDetection
{
    classifier = [[RDFClassifier alloc] initWithLearningSamples:[RKTrainingSet trainingSet]
                                                numberOfSamples:SAMPLES_NUMBER
                                               numberOfFeatures:FEATURES_COUNT
                                                numberOfClasses:CLASSES_COUNT
                                                  numberOfTrees:TREES_NUMBER
                                                          ratio:TRAINING_RATIO];
    [self setDetection:YES];
    
    counter = -1;
    
    [motionManager startDeviceMotionUpdatesToQueue:motionQueue
                                       withHandler:^(CMDeviceMotion *motion, NSError *error)
     {
         [self handleMotion:motion error:error];
     }];
}

-(void) endMovementDetection
{
    [motionManager stopDeviceMotionUpdates];
    
    [self setDetection:NO];
    
    classifier = nil;
}

- (void) setDetection:(BOOL)inProgress
{
    [detectionLock lock];
    
    detecting = inProgress;
    
    [detectionLock unlock];
}

//------------------------------------------
// Public methods
//------------------------------------------
#pragma mark - Public methods

- (void) detectMovementWithCompletionBlock:(void (^)(NSArray *,RKMovementType))completionBlock
{
    if (!detecting)
    {
        handlerBlock = [completionBlock copy];
        
        [self beginMovementDetection];
    }
}

- (void) cancelMovementDetection
{
    [self endMovementDetection];
}

- (BOOL) detectionInProgress
{
    return detecting;
}

@end
