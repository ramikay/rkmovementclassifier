//
//  RDFClassifier.m
//  RKMovementTest
//
//  Created by Rami on 26/3/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#import "RDFClassifier.h"
#import "dataanalysis.h"

@interface RDFClassifier ()
{
    alglib::decisionforest df;
    alglib::dfreport dfreport;

    int nbSamples;
    int nbFeatures;
    int nbClasses;
}
@end

@implementation RDFClassifier

- (id) initWithLearningSamples:(double *)theLearningSamples
               numberOfSamples:(int)nSamples
              numberOfFeatures:(int)nFeatures
               numberOfClasses:(int)nClasses
                 numberOfTrees:(int)nTrees
                         ratio:(double)theRatio
{
    self = [super init];
    
    if (self != nil)
    {
        alglib_impl::ae_int_t info = 0;
        
        nbClasses = nClasses;
        nbSamples = nSamples;
        nbFeatures = nFeatures;
        
        df = *new alglib::decisionforest();
        dfreport = *new alglib::dfreport();
        
        alglib::real_2d_array xy;
        xy.setcontent(nbSamples, nbFeatures+1, theLearningSamples);
        
        alglib::dfbuildrandomdecisionforest(xy,
                                            nbSamples,
                                            nbFeatures,
                                            nbClasses,
                                            nTrees,                                            
                                            theRatio,
                                            info,
                                            df,
                                            dfreport);
        
        if (info != 1)
        {
            NSLog(@"Random Forest Creation not solved");
        }
    }
    
    return self;
}

- (NSArray *) classifyData:(NSArray *)theData
{
    alglib::real_1d_array data;
    data.setlength([theData count]);
    
    for(int i = 0; i < [theData count]; i++)
    {
        data[i] = [[theData objectAtIndex:i] doubleValue];
    }
    
    alglib::real_1d_array result = *new alglib::real_1d_array();
    result.setlength(nbClasses);
    
    alglib::dfprocess(df, data, result);
    
    NSMutableArray *_values = [[NSMutableArray alloc] initWithCapacity:nbClasses];
    for(int i = 0; i < nbClasses; i++)
    {
        [_values addObject:[NSNumber numberWithDouble:result[i]]];
    }
    
    return _values;
}

@end
