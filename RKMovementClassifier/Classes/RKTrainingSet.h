//
//  RKTrainingSet.h
//  RKMovementTest
//
//  Created by Rami on 7/4/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

@interface RKTrainingSet : NSObject

+ (double *) trainingSet;

@end