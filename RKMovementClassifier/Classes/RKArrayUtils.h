//
//  RKArrayUtils.h
//  RKMovementTest
//
//  Created by Rami on 7/4/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#ifndef Test_RKArrayUtils_h
#define Test_RKArrayUtils_h

double max_array(double a[], int num_elements);

double min_array(double a[], int num_elements);

double avg_array(double a[], int num_elements);

double std_dev_array(double a[], int num_elements, double mean);

#endif
