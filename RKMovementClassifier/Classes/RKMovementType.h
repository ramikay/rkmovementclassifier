//
//  RKMovementType.h
//  RKMovementTest
//
//  Created by Rami on 7/4/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

typedef enum
{
    RKMovementTypeUnknown=-1,
    RKMovementTypeStationary=0,
    RKMovementTypePedestrian=1,
    RKMovementTypeVehicle=2,
    RKMovementTypeBicycle=3,
    RKMovementTypeMotorbike=4
} RKMovementType;
