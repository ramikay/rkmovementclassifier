//
//  RKMovementConstants.h
//  RKMovementTest
//
//  Created by Rami on 7/4/13.
//  Copyright (c) 2013 Rami Khawandi. All rights reserved.
//

#ifndef Test_RKMovementConstants_h
#define Test_RKMovementConstants_h

#define FEATURES_COUNT          12      // Number of features in each feature set
#define CLASSES_COUNT           5       // Number of different classes

#define SAMPLES_NUMBER          1198    // Rows inside training set

#define UPDATE_INTERVAL         0.8     // Time interval (in seconds) of when the motion manager updates sensor values
#define FEATURE_SET_SIZE        10      // Size of the feature set

#define TREES_NUMBER            50      // Number of trees to be used. Must be > 1. recommended values: 50-100
#define TRAINING_RATIO          0.66    // Percentage of the training set to be used. 0<R<=1. recommended values: 0.1 <= R <= 0.66

#endif
